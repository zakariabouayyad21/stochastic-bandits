import numpy as np
import random
from scipy.stats import entropy, norm
from tqdm import tqdm
import pandas as pd


def generate_bernouilli_variables(number_of_arms):
    bernoulli_variables = np.random.dirichlet(np.ones(number_of_arms), size=1)
    return bernoulli_variables[0]


def simulate_bernouilli_experience(conversion_rate):
    if np.random.rand() < conversion_rate:
        return 1
    else:
        return 0


def simulate_bernouilli_round(conversion_rates):
    simulated_round_data = [
        simulate_bernouilli_experience(conversion_rate)
        for conversion_rate in conversion_rates
    ]
    return simulated_round_data


def generate_bernoulli_simulation(number_of_rounds, conversion_rates):

    rounds_array = np.array(
        [simulate_bernouilli_round(conversion_rates) for i in range(number_of_rounds)]
    )

    bernouilli_simulation = pd.DataFrame(
        {"Arm " + str(i): rounds_array[:, i] for i in range(len(conversion_rates))}
    )
    return bernouilli_simulation
