import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy import stats
import seaborn as sns
import sys
import os

sys.path.insert(0, os.path.dirname(os.getcwd()))

from modeling.upper_confidence_bound import UCB_sub_gaussian


def compute_strategy_regret(number_of_rounds, selected_arms, arm_means):
    means_per_round = [arm_means[selected_arm] for selected_arm in selected_arms]

    regret = number_of_rounds * max(arm_means)
    regret = regret - sum(means_per_round)

    return regret


def compute_instance_worst_possible_regret(number_rounds, arm_means):
    worst_arm_index = arm_means.index(min(arm_means))

    worst_possible_selections = [worst_arm_index for i in range(number_rounds)]

    worst_possible_regret = compute_strategy_regret(
        number_rounds, worst_possible_selections, arm_means
    )

    return worst_possible_regret


def compute_immediate_regrets(rewards, best_arm_mean):
    try:
        flattened_rewards = [reward for sublist in rewards for reward in sublist]
    except:
        flattened_rewards = rewards.copy()
    immediate_regrets = []
    for i in range(len(flattened_rewards)):
        current_empirical_mean = np.mean(flattened_rewards[: i + 1])
        immediate_regrets.append(best_arm_mean - current_empirical_mean)
    return immediate_regrets


def plot_immediate_regrets(list_of_immediate_regrets, labels):
    plt.figure(figsize=(15, 10))
    for i in range(len(list_of_immediate_regrets)):
        plt.plot(list_of_immediate_regrets[i], label=labels[i])
    plt.legend()
    plt.xlabel("Number of rounds")
    plt.ylabel("Regret")


def plot_arm_selection_histogram(selected_arms):
    plt.figure(figsize=(13, 9))
    plt.hist(selected_arms)
    plt.xlabel("Arms")
    plt.ylabel("Number of Selections")


################# UCB Specific Functions #####################


def plot_upper_confidence_bounds_decrease(upper_confidence_bounds, figsize=(17, 15)):

    plt.figure(figsize=figsize)
    for i in range(upper_confidence_bounds.shape[0]):
        plt.plot(upper_confidence_bounds[i][1:], label="Arm {}".format(i))
    plt.legend(prop={"size": 17})
    plt.xlabel("Number of Selections")
    plt.ylabel("Upper Confidence Bound")


def plot_regret_versus_UCB_probability_error(
    dataset, selected_arms, probability_errors=np.linspace(0.001, 1, 30)
):

    all_ucb_rewards = []
    all_ucb_selected_arms = []

    for probability_error in tqdm(probability_errors):
        ucb_rewards, ucb_selected_arms, upper_confidence_bounds = UCB_sub_gaussian(
            dataset.values, probability_error
        )
        all_ucb_rewards.append(ucb_rewards)
        all_ucb_selected_arms.append(ucb_selected_arms)

    regrets_in_function_of_error = pd.Series(
        data=[
            compute_strategy_regret(len(dataset), ucb_selected_arms, selected_arms)
            for ucb_selected_arms in all_ucb_selected_arms
        ],
        index=probability_errors,
    )

    plt.figure(figsize=(9, 9))
    regrets_in_function_of_error.plot()
    plt.xlabel("Error Probability")
    plt.ylabel("Regret")


##################### Thompson Sampling Specific Functions #####################


def plot_posteriors(beta_priors, ax=None, title=None):
    # fig = plt.figure(figsize=(12.5, 10))

    x = np.linspace(0.001, 1, 150)
    for i, (alpha, beta) in enumerate(beta_priors):
        # color = assets[i]
        y = stats.beta.rvs(alpha, beta, size=10000)
        """
        lines = sns.lineplot(
            x, y, label="%s (%.1f,%.1f)" % (color, alpha, beta), color=color, ax=ax
        )
        """
        lines = sns.lineplot(x, y, label="(%.1f,%.1f)" % (alpha, beta), ax=ax)
        # lines = sns.distplot(y)
        # plt.fill_between(x, 0, y, alpha=0.2, color=color)
        if title:
            plt.title(title)
        plt.autoscale(tight=True)
    plt.legend(title=r"$\alpha, \beta$ - beta_priors")
    return plt


"""
test = [(1, 2), (1, 3), (4, 6)]

plot_posteriors(test)
"""


def plot_thompson_samping_beta_parameters_evolution():
    fig = plt.figure(figsize=(40, 60))
    fig.subplots_adjust(hspace=0.4, wspace=0.4)
    cnt = 1
    for i in range(0, 30, 2):
        ax = fig.add_subplot(8, 4, cnt)
        g = plot_posteriors(*data[i][1:], ax, "after " + str(i) + " runs")
        cnt += 1
    plt.show()
