import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


def chernoff(n, sigma, epsilon):
    num_in_exp = -1 * n * epsilon ** 2
    deno_in_exp = 2 * sigma ** 2
    res = num_in_exp / deno_in_exp
    res = np.exp(res)
    return res


def tchebytchev(n, sigma, epsilon):
    numerator = sigma ** 2
    denominator = n * epsilon ** 2
    res = numerator / denominator
    return res


def hoeffding(n, epsilon):
    numerator = -2 * n * (epsilon ** 2)
    res = np.exp(numerator)
    return res


def plot_concentration_inequalities_bounds(
    number_rounds, sigma, epsilon, figsize=(9, 9), ylim_min=0, ylim_max=1
):

    sns.set()
    chernoff_data = [
        chernoff(iterable, sigma, epsilon) for iterable in np.arange(10, number_rounds)
    ]
    tchebytchev_data = [
        tchebytchev(iterable, sigma, epsilon)
        for iterable in np.arange(10, number_rounds)
    ]
    hoeffding_data = [
        hoeffding(iterable, epsilon) for iterable in np.arange(10, number_rounds)
    ]
    plt.figure(figsize=figsize)
    plt.plot(chernoff_data, label="Chernoff Bound")
    plt.plot(tchebytchev_data, label="Tchebytchev Bound")
    plt.plot(hoeffding_data, label="Hoeffding Bound")
    plt.ylim(0, 1)
    plt.legend()
    plt.xlabel("Number of Rounds")
    plt.ylabel("Tail Probability Bound")
    plt.title(
        "Tail Probability Bound Decrease for epsilon = "
        + str(epsilon)
        + " sigma = "
        + str(sigma)
    )
