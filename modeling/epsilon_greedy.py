# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
from random import randrange
from scipy.stats import entropy
from tqdm import tqdm
import pandas as pd
from numpy.random import uniform, choice


def identify_current_best_arm(rewards_per_arm):

    mean_rewards_per_arm = {
        arm: np.mean(rewards) for (arm, rewards) in rewards_per_arm.items()
    }

    current_best_arm = max(mean_rewards_per_arm, key=mean_rewards_per_arm.get)

    return current_best_arm


def select_arm_with_epsilon_greedy_strategy(number_of_arms, epsilon, current_best_arm):

    if uniform(0, 1) < epsilon:
        selected_arm = current_best_arm
    else:
        other_arms = [arm for arm in range(number_of_arms) if arm != current_best_arm]
        selected_arm = choice(other_arms)
    return selected_arm


def epsilon_greedy(dataset, epsilon):

    number_of_rounds = dataset.shape[0]
    number_of_arms = dataset.shape[1]
    rewards_per_arm = {arm: [] for arm in range(number_of_arms)}
    selected_arms = []

    for episode in tqdm(range(number_of_rounds)):
        if episode < number_of_arms:
            selected_arm = episode
        else:
            current_best_arm = identify_current_best_arm(rewards_per_arm)
            selected_arm = select_arm_with_epsilon_greedy_strategy(
                number_of_arms, epsilon, current_best_arm
            )

        selected_arms.append(selected_arm)
        rewards_per_arm[selected_arm].append(dataset[episode][selected_arm])

    return rewards_per_arm, selected_arms
