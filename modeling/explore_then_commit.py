# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
from random import randrange
from scipy.stats import entropy
from tqdm import tqdm
import pandas as pd


# AB Testing strategy choice


def get_exploration_selected_strategies(rounds_array, number_exploration_rounds):

    exploration_selected_strategies = [
        randrange(rounds_array.shape[1]) for episode in range(number_exploration_rounds)
    ]

    return exploration_selected_strategies


def get_exploration_rewards(rounds_array, exploration_selected_strategies):

    exploration_rewards = [
        rounds_array[current_round, selected_arm]
        for (current_round, selected_arm) in zip(
            range(len(rounds_array)), exploration_selected_strategies
        )
    ]

    return exploration_rewards


def get_best_arm_after_exploration(
    exploration_selected_strategies, exploration_rewards
):

    exploration_summary = pd.DataFrame(
        {"Selected Arm": exploration_selected_strategies, "Reward": exploration_rewards}
    )

    total_rewards_per_arm = exploration_summary.groupby("Selected Arm")["Reward"].max()

    best_arm_post_exploration = total_rewards_per_arm.idxmax()

    return best_arm_post_exploration


def get_exploitation_rewards(
    rounds_array, arm_for_exploitation, number_exploration_rounds
):

    exploitation_rewards = [
        rounds_array[current_round, arm_for_exploitation]
        for current_round in range(number_exploration_rounds, len(rounds_array))
    ]

    return exploitation_rewards


def explore_then_commit(rounds_array, number_exploration_rounds):

    exploration_selected_strategies = get_exploration_selected_strategies(
        rounds_array, number_exploration_rounds
    )

    exploration_rewards = get_exploration_rewards(
        rounds_array, exploration_selected_strategies
    )

    arm_for_exploitation = get_best_arm_after_exploration(
        exploration_selected_strategies, exploration_rewards
    )

    exploitation_rewards = get_exploitation_rewards(
        rounds_array, arm_for_exploitation, number_exploration_rounds
    )

    rewards = exploration_rewards.copy()

    rewards.extend(exploitation_rewards)

    selected_arms = exploration_selected_strategies.copy()

    selected_arms.extend(
        [arm_for_exploitation for i in range(len(rewards) - len(exploration_rewards))]
    )

    return rewards, selected_arms
