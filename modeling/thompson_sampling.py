import numpy as np
import random
from tqdm import tqdm

# Thompson Sampling strategy
def thompson_sampling(rounds_array, conversion_rates):
    selected_strategies = []
    rewards = [[] for i in range(rounds_array.shape[0])]
    numbers_of_positive_rewards = np.zeros(rounds_array.shape[1])
    numbers_of_null_rewards = np.zeros(rounds_array.shape[1])
    for episode in range(rounds_array.shape[0]):
        best_thompson_sampling_strategy = 0
        highest_beta_draw = 0
        # Finding the best strategy according to Thompson Sampling for this round
        for arm in range(rounds_array.shape[1]):
            beta_law_draw = random.betavariate(
                numbers_of_positive_rewards[arm] + 1, numbers_of_null_rewards[arm] + 1
            )
            if beta_law_draw > highest_beta_draw:
                highest_beta_draw = beta_law_draw
                best_thompson_sampling_strategy = arm
        # Updating the number of rewards used to sample from the beta distribution
        selected_strategies.append(best_thompson_sampling_strategy)
        reward = rounds_array[episode, best_thompson_sampling_strategy]
        rewards[best_thompson_sampling_strategy].append(reward)
        if reward == 1:
            numbers_of_positive_rewards[best_thompson_sampling_strategy] += 1
        else:
            numbers_of_null_rewards[best_thompson_sampling_strategy] += 1
    return rewards, selected_strategies
