# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import random
from scipy.stats import entropy
from tqdm import tqdm


# Upper Condifence Bound : Sub-Gaussian reward distribution


def initialize_upper_confidence_bounds(initial_ucb_value, number_of_arms):
    upper_confidence_bounds = [initial_ucb_value for i in range(number_of_arms)]
    return upper_confidence_bounds


def update_previous_arm_ucb(
    arm_choice_frequency, selected_arm, error_probability, rewards
):
    # Updating the confidence interval of only the previously selected arm

    new_exploration_bonus = (
        2 * np.log(1 / error_probability) / arm_choice_frequency[selected_arm]
    )
    new_exploration_bonus = np.sqrt(new_exploration_bonus)
    new_upper_confidence_bound = np.mean(rewards[selected_arm]) + new_exploration_bonus

    return new_upper_confidence_bound


def select_arm_with_UCB_sub_gaussian_strategy(
    current_episode, number_of_arms, current_upper_confidence_bounds
):
    if current_episode < number_of_arms:
        selected_arm = current_episode
    else:
        selected_arm = np.argmax(current_upper_confidence_bounds)
    return selected_arm


### Maybe create a function that takes previous UCB values and returns a new one ? Even if only one value changes ? It will only call upon the previous function


def UCB_sub_gaussian(rounds_array, error_probability, initial_ucb_value=10 ** 9):

    selected_arms = []
    upper_confidence_bounds = [[] for i in range(rounds_array.shape[1])]
    rewards = [[] for i in range(rounds_array.shape[1])]
    arm_choice_frequency = np.zeros(
        rounds_array.shape[1]
    )  # Should this even exist given that you can get this info from the selected arms

    number_of_arms = rounds_array.shape[1]
    current_upper_confidence_bounds = initialize_upper_confidence_bounds(
        initial_ucb_value, number_of_arms
    )

    for episode in range(rounds_array.shape[0]):
        if episode >= rounds_array.shape[1]:
            # Updating the confidence interval of only the previously selected arm

            current_upper_confidence_bounds[selected_arm] = update_previous_arm_ucb(
                arm_choice_frequency, selected_arm, error_probability, rewards
            )
            ## The selected arms can be easily deduced from the current upper_confidence_bounds
            ### These two next quantities are the single two actions that are really important in the function
            selected_arm = np.argmax(current_upper_confidence_bounds)
            upper_confidence_bounds[selected_arm].append(
                current_upper_confidence_bounds[selected_arm]
            )
        else:
            selected_arm = episode
        selected_arms.append(selected_arm)
        #### The arm choice frequency is very easy to deduce from the selected arms list
        arm_choice_frequency[selected_arm] += 1
        episode_reward = rounds_array[episode][selected_arm]
        #### This is the problem the particular structure of the reward
        rewards[selected_arm].append(episode_reward)
    return rewards, selected_arms, np.array(upper_confidence_bounds)
